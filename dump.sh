#!/bin/sh
# This script will handle the dump functions of TCESP Project

today=$(date +'%Y-%m-%d')

PKG_OK=$(dpkg-query -W --showformat='${Status}\n' zenity|grep "install ok installed")
echo Checking for zenity: ${PKG_OK}
if [ "" == "$PKG_OK" ]; then
  echo "No zenity. Setting up zenity."
  gksudo -m "Insira sua senha para que possamos instalar o zenity" "apt-get --force-yes --yes install zenity"
fi

PKG_OK=$(dpkg-query -W --showformat='${Status}\n' sshpass|grep "install ok installed")
echo Checking for sshpass: ${PKG_OK}
if [ "" == "$PKG_OK" ]; then
  echo "No sshpass. Setting up sshpass."
  gksudo -m "Insira sua senha para que possamos instalar o sshpass" "apt-get --force-yes --yes install sshpass"
fi

login=$(zenity --forms --title="Informações do servidor" --text="Digite aqui as informações de login do servidor" --add-entry="Endereço do servidor" --add-entry="Usuário do servidor" --add-password="Senha do servidor")

address=$(echo "${login}" | awk -F "|" '{print  $1}')
user=$(echo "${login}" | awk -F "|" '{print  $2}')
passwd=$(echo "${login}" | awk -F "|" '{print  $3}')

folder=$(zenity --file-selection --directory --title="Em que pasta os dumps serão salvos?")

echo "pg_dump -U postgres tcesp_dev > /var/www/public_html/dump_tcesp/dump_tcesp_dev_$today.sql; \
      pg_dump -U postgres etcesp_dev > /var/www/public_html/dump_tcesp/dump_etcesp_dev_$today.sql; \
      pg_dump -U postgres licitacao_dev > /var/www/public_html/dump_tcesp/dump_licitacao_dev_$today.sql; \
      pg_dump -U postgres ecp_dev > /var/www/public_html/dump_tcesp/dump_ecp_dev_$today.sql;" | sshpass -p ${passwd} ssh ${user}@${address}

echo "Todos os dumps foram criados. Eles serão enviados para a sua máquina agora \n"

sshpass -p ${passwd} scp ${user}@${address}:/var/www/public_html/dump_tcesp/dump_*_dev_${today}.sql ${folder}

echo "Arquivos criados, vamos agora removê-los! \n"

echo "rm /var/www/public_html/dump_tcesp/dump_*_dev_${today}.sql" | sshpass -p ${passwd} ssh ${user}@${address}

echo "Pronto :) Obrigado por usar este lindo programinha do time Solfoda ;)"